﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace winstore_dashboard_automaton.Xml
{
    public class OptionsDeserializer
    {
        public static AppOptions Parse(string optionsFilepath)
        {
            XmlSerializer ser = new XmlSerializer(typeof(AppOptions));
            AppOptions options;
            using (XmlReader reader = XmlReader.Create(optionsFilepath))
            {
                options = (AppOptions)ser.Deserialize(reader);
            }
            return options;
        }
    }
}
