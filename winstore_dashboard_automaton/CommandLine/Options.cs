﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winstore_dashboard_automaton.CommandLine
{
    public class Options
    {
        [Option('o', "options", Required = true,
        HelpText = "Options file")]
        public string OptionsFilepath { get; set; }           
    }
}
