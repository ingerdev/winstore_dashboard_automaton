﻿using winstore_dashboard_automaton.WebCrawler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winstore_dashboard_automaton.CommandLine
{
    //takes parsed options class and create proper set of tasks for web crawler
    public class MarketTaskGenerator
    {        
        public static MarketCrawler GenerateCrawlerWithTasks(AppOptions options)
        {
           // if (options.Test)
           // {
                return generateCrawler(options);                
           // }
           // return null;
        }


        private static MarketCrawler generateCrawler(AppOptions options)
        {
            WebDriverEnum selectedBrowser = mapBrowserStringToEnum(options.GlobalDetails.BrowserDriver);
            if (selectedBrowser == WebDriverEnum.Unknown)
                selectedBrowser = WebDriverEnum.Chrome;

            MarketCrawler crawler = new MarketCrawler(selectedBrowser);

            //add login task
            crawler.addTask(new LoginPageCrawler(options.GlobalDetails.Login,options.GlobalDetails.Password));

            //add app selection task
            crawler.addTask(new DashboardCrawler(options.GlobalDetails.ApplicationName));

            //create and add list of tasks that will operate on each language page
            List<IMarketWorkUnit> marketWorkers = new List<IMarketWorkUnit>();
            foreach (var task in options.Tasks)
            {
                marketWorkers.Add(MarketWorkUnitFactory.Create(task));
            }

            crawler.addTask(new LanguagePageCrawler(marketWorkers));
            
           return crawler;
        }

        private static WebDriverEnum mapBrowserStringToEnum(MainParametersBrowserDriver browser)
        {           
            switch(browser)
            {
                case MainParametersBrowserDriver.InternetExplorer: return WebDriverEnum.IE;
                case MainParametersBrowserDriver.Firefox: return WebDriverEnum.Firefox;
                case MainParametersBrowserDriver.Chrome: return WebDriverEnum.Chrome;
                case MainParametersBrowserDriver.Edge: return WebDriverEnum.Edge;
                default: return WebDriverEnum.Unknown;
            }
        }
    }
}
