﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using winstore_dashboard_automaton.CommandLine;
using winstore_dashboard_automaton.WebCrawler;
using winstore_dashboard_automaton.Xml;

namespace winstore_dashboard_automaton
{
    class Program
    {
        enum ParseError { NoError = 0,Error}
        static void Main(string[] args)
        {
            //parsing command line options
            Parser parser = new Parser();
            var result = parser.ParseArguments<Options>(args);
            Tuple<ParseError,Options> parsedOptions = result.MapResult(options =>
            {
                //use parsed options here
                //...
                //or return them in tuple
                return new Tuple<ParseError,Options>(ParseError.NoError,options);
            },
                errors =>
            {
                Console.WriteLine("Invalid parameters. Type -h for help");
                return new Tuple<ParseError, Options>(ParseError.Error, null);
            });
            if (parsedOptions.Item1 != ParseError.NoError)
                return;

            //setup our drivers 
            //setup environment variables not work in windows so dont do this
            //WebDriversSetup.Setup();
            AppOptions xml_options = OptionsDeserializer.Parse(parsedOptions.Item2.OptionsFilepath);
            //we have set of options
            //create market crawler and send options to it
            using (var crawler = MarketTaskGenerator.GenerateCrawlerWithTasks(xml_options))
            {
                //start crawler
                crawler.ProcessTasks();
            }

              
            
        }
    }
}
