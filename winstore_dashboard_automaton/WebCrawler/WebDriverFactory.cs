﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using System;

namespace winstore_dashboard_automaton.WebCrawler
{
    public static class WebDriverFactory
    {
        public static IWebDriver CreateWebDriver(WebDriverEnum webDriver)
        {
            switch (webDriver)
            {
                case WebDriverEnum.Chrome:
                    return createChromeDriver();                    
                case WebDriverEnum.Firefox:
                    return createFirefoxDriver();
                case WebDriverEnum.IE:
                    return createIEDriver();
                case WebDriverEnum.Edge:
                    return createEdgeDriver();
                case WebDriverEnum.Unknown:
                default:
                    throw new ArgumentException("Cannot create unknown web driver");                
            }
        }

        private static IWebDriver createEdgeDriver()
        {
            var dir = getCurrentAppDirectory();           
            var driver = new EdgeDriver(dir + @"\WebDrivers\EdgeDriver"); ;
            //focus driver window
            //driver.ExecuteScript("alert(\"Focus window\")");
            return driver;
        }

        private static IWebDriver createIEDriver()
        {
            throw new NotImplementedException();
        }

        private static IWebDriver createFirefoxDriver()
        {
            throw new NotImplementedException();
        }

        private static IWebDriver createChromeDriver()
        {
            var dir = getCurrentAppDirectory();
            var driver =  new ChromeDriver(dir + @"\WebDrivers\ChromeDriver"); ;
            //focus driver window
            //driver.ExecuteScript("alert(\"Focus window\")");
            return driver;
        }

        private static string getCurrentAppDirectory()
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }
    }
}
