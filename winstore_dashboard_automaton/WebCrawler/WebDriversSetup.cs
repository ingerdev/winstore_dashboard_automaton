﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace winstore_dashboard_automaton.WebCrawler
{
    //settup all necessary environment variables
    //to be sure webdrivers will be used correctly
    public static class WebDriversSetup
    {

        public static void Setup()
        {
            setupChromeDriver();
            //setupFireFoxDriver();
            setupEdgeDriver();
            //setupIEDriver();
        }

        private static void setupIEDriver()
        {
            throw new NotImplementedException();
        }

        private static void setupEdgeDriver()
        {           
            var dir = getCurrentAppDirectory();
            Environment.SetEnvironmentVariable("webdriver.edge.driver",
                dir + @"\WebDrivers\EdgeDriver\microsoftwebdriver.exe",
                EnvironmentVariableTarget.Machine);
        }

        private static void setupFireFoxDriver()
        {
            throw new NotImplementedException();
        }

        //this method require admin right to overwrite environment variable
        //[PrincipalPermission(SecurityAction.Demand, Role = @"BUILTIN\Administrators")]
        private static void setupChromeDriver()
        {
            var dir = getCurrentAppDirectory();
            Environment.SetEnvironmentVariable("webdriver.chrome.driver",
                dir + @"\WebDrivers\ChromeDriver\chromedriver.exe",
                EnvironmentVariableTarget.Machine);
        }
        private static string getCurrentAppDirectory()
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }
    }
}
