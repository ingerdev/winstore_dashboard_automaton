﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winstore_dashboard_automaton.WebCrawler
{
    public static class MarketWorkUnitFactory
    {
        
        public static IMarketWorkUnit Create(TaskDescription task)
        {
            if (task is IconChangeTask)
            {
                IconChangeTask taskDescription = (task as IconChangeTask);
                return new ChangeAppIconCrawler(taskDescription.NewIconPath);
            }
            throw new ArgumentException("Unknown task type");
                  
        }

    

    }
}
