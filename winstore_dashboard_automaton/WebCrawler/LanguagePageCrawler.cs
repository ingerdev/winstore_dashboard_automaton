﻿using winstore_dashboard_automaton.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winstore_dashboard_automaton.WebCrawler
{
    public class LanguagePageCrawler : IMarketWorkUnit
    {

        private IReadOnlyList<IMarketWorkUnit> _languageWorkUnits;

        public LanguagePageCrawler(List<IMarketWorkUnit> languageWorkUnits)
        {
            _languageWorkUnits = languageWorkUnits.AsReadOnly();
        }

        public void Process(IWebDriver web)
        {

            //getting block containing language items

            foreach (var languageName in parseLanguageNames(web))
            {
                IWebElement language = getLanguageElement(languageName, web);

                language.Click();
                foreach (var langWorkUnit in _languageWorkUnits)
                {
                    langWorkUnit.Process(web);
                }
                //save changes
                web.FindElement(By.XPath("//button[contains(.,'Save')]")).Click();
            }
            //found description section                 
        }

        private IWebElement getLanguageElement(string languageName, IWebDriver web)
        {
            var languages = getLanguagesElements(web);
            return languages.First(lang => lang.GetAttribute("title").Equals(languageName));
        }

        private List<string> parseLanguageNames(IWebDriver web)
        {
            List<String> langNames = new List<string>();
            foreach(var langElement in getLanguagesElements(web))
            {
                langNames.Add(langElement.GetAttribute("title"));
            }
            return langNames;
        }
        private IReadOnlyCollection<IWebElement> getLanguagesElements(IWebDriver web)
        {
            //getting block containing language items
            web.WaitForElementLoad(By.ClassName("task-detail-list"));
            var section = web.FindElement(By.XPath("//h3[contains(text(),'Descriptions')]")).FindElement(By.XPath("parent::*"));

            return section.FindElement(By.ClassName("task-detail-list")).FindElements(By.ClassName("item-title"));
        }
    }
}
