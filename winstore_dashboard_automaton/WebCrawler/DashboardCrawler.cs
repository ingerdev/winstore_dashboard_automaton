﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using winstore_dashboard_automaton.Helpers;
using System.Collections.ObjectModel;

namespace winstore_dashboard_automaton.WebCrawler
{
    public class DashboardCrawler : IMarketWorkUnit
    {
        private string _appName;
        public DashboardCrawler(string appName)
        {
            _appName = appName;
        }

        public void Process(IWebDriver web)
        {
            //1) navigate appPage
            navigateAppPage(web);
            //2) updateSubmission click
            updateSubmission(web);
        }

        private void navigateAppPage(IWebDriver web)
        {
            //1) click "View all apps"
            web.WaitForElementLoad(By.LinkText("View all apps"));
            web.FindElement(By.LinkText("View all apps")).Click();

            //2) find app by name and click it
            //TODO: iterate over apps list         
            web.WaitForElementLoad(By.Id("appLink_" + _appName));
            //clicking item mean we going to its submission page
            web.FindElement(By.Id("appLink_" + _appName)).Click();
        }

        //clicking to updateSubmission button
        private void updateSubmission(IWebDriver web)
        {
            //get sumbissions table
            web.WaitForElementLoad(By.CssSelector("table.submissions"));
            var tableSubmissions = web.FindElement(By.CssSelector("table.submissions")).FindElement(By.TagName("tbody"));

            //find submission with "update" status
            ReadOnlyCollection<IWebElement> allRows = tableSubmissions.FindElements(By.TagName("tr"));              
            for (int z = 0; z < allRows.Count; z++)
            {
                ReadOnlyCollection<IWebElement> cells = allRows[z].FindElements(By.TagName("td"));

                if (cells.Count == 0)
                    continue;

                if (string.IsNullOrEmpty(cells[0].GetAttribute("id")))
                    continue;

                //this can be found in completed submission
                if (cells[3].Text.Equals("Delete"))
                {
                    //we found existing non-complete submission
                    //so click to submission name
                    cells[0].FindElement(By.TagName("a")).Click();
                    return;
                }

                if (cells[3].Text.Equals("Update"))
                {
                    //we found complete submission
                    //so click to update it 
                    cells[3].FindElement(By.TagName("a")).Click();                  
                    return;
                }


            }

        }

    }
}
