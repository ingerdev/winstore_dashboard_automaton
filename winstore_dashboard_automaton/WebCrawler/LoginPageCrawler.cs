﻿using winstore_dashboard_automaton.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace winstore_dashboard_automaton.WebCrawler
{
    public class LoginPageException:Exception
    {
        public enum LoginErrorEnum {WrongLoginPassword,UnspecifiedError}
        public LoginPageException(string message):base(message)
        {

        }
    }
    public class LoginPageCrawler:IMarketWorkUnit
    {
        
        private IWebDriver _web;
        private string _login, _password;
        public LoginPageCrawler(string login, string password)
        {
            _login = login;
            _password = password;
        }
        public void Process(IWebDriver webDriver)
        {
            _web = webDriver;
            login(_login, _password);
        }

        private void login(string login, string password)
        {
            try
            {
                //try to login without credentials                
                //loginWithoutCredentials();
                //return;
            }
            catch(LoginPageException)
            {
                //cannot login without credentials,
                //this exception dont need special handling
                //as below we use another login method
            }

            //prepare full-size login
            loginWithCredentials(login, password);

        }

        const string START_PAGE_URL = "https://developer.microsoft.com/en-us/windows";
        //login 
        private void loginWithoutCredentials()
        {
            //1)open https://developer.microsoft.com/en-us/windows
            _web.Url = START_PAGE_URL;
            _web.Navigate();

            //2)clicking "dashboard"      
            var dashboard = _web.FindElement(By.Id("dashboard"));
            dashboard.Click();
        }

        private void loginWithCredentials(string login, string password)
        {
            //0) focus window
           // _web.ExecuteScript('alert("Focus window")'))           
            //1) open https://developer.microsoft.com/en-us/windows
            _web.Url = START_PAGE_URL;
            _web.Navigate();

            //2) clicking "dashboard"    
            _web.WaitForElementLoad(By.Id("dashboard"));
            var dashboard = _web.FindElement(By.Id("dashboard"));
            dashboard.Click();

            //3) find userid field and type here our login
            var loginField = _web.FindElement(By.Id("cred_userid_inputtext"));
            loginField.SendKeys(login);

            //4 find password field and type here our password
            var passField = _web.FindElement(By.Id("cred_password_inputtext"));
            passField.SendKeys(password);

            Thread.Sleep(2000);

            //5 find "Submit" button and click on it
            _web.WaitForElementLoad(By.Id("cred_sign_in_button"));
            var submitButton = _web.FindElement(By.Id("cred_sign_in_button"));
            
            if (submitButton.Enabled)
                submitButton.Click();
        }
    }
}
