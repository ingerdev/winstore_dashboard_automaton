﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winstore_dashboard_automaton.WebCrawler
{
    public interface IMarketWorkUnit
    {
       void Process(IWebDriver web);
    }
}
