﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using winstore_dashboard_automaton.Helpers;
using System.Threading;
using winstore_dashboard_automaton.AutoItTasks;

namespace winstore_dashboard_automaton.WebCrawler
{
    public class ChangeAppIconCrawler : IMarketWorkUnit
    {
        string _newIconPath;
        public ChangeAppIconCrawler(string newIconPath)
        {
            _newIconPath = newIconPath;
        }
        public void Process(IWebDriver web)
        {
            //deleteExistingIcon(web);
            selectNewIcon(web);
        }       

        private void deleteExistingIcon(IWebDriver web)
        {
            try
            {
                //web.WaitForElementLoad(By.XPath("//*[contains(@id, 'wp-phoneIcon-options-delete-a-')]"));
                var deleteButton = web.FindElement(By.XPath("//*[contains(@id, 'wp-phoneIcon-options-delete-a-')]"));
                if (deleteButton.Displayed)
                    deleteButton.Click();
            }
            catch(NoSuchElementException)
            {
                //we failed with deleting current appIcon that
                //*possible* mean we dont have existing icon at all.
                //ignore this exception as no action is required                
            }
        }

        private void selectNewIcon(IWebDriver web)
        {           
            //open FilePicker dialog  
            web.WaitForElementLoad(By.XPath("//*[contains(@id, 'wp-phoneIcon-div-')]"));
            web.FindElement(By.XPath("//*[contains(@id, 'wp-phoneIcon-div-')]")).Click();
            //web.FillFileDialog2(_newIconPath);
            //Thread.Sleep(5000);
            AutoItHelper.ChromeDriverSetFiledialogFilename(_newIconPath);
        }
    }
}
