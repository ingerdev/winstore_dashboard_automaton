﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winstore_dashboard_automaton.WebCrawler
{
    public class MarketCrawlerException : Exception
    {
        public MarketCrawlerException(string message):base(message)
        {

        }
    }

    public enum WebDriverEnum {Chrome,Firefox,IE,Edge,Unknown}
    public class MarketCrawler: IDisposable
    {
        List<IMarketWorkUnit> _marketTasks = new List<IMarketWorkUnit>();

        //!!! this variable contain state !!!
        //it is webbrowser which can currently be navigated to various pages
        IWebDriver _web;

        public MarketCrawler(WebDriverEnum selectedWebDriver)
        {
            _web = WebDriverFactory.CreateWebDriver(selectedWebDriver);
            /* 
            switch(selectedWebDriver)
            {
                case WebDriverEnum.Chrome:
                    _web = new ChromeDriver();
                    break;
                case WebDriverEnum.Firefox:
                    _web = new FirefoxDriver();
                    break;
                case WebDriverEnum.IE:
                    _web = new InternetExplorerDriver();
                    break;
                case WebDriverEnum.Edge:
                    _web = new EdgeDriver();
                    break;
                default: throw
                        new MarketCrawlerException($"No idea how to create selected driver {selectedWebDriver.ToString()}");
            }   
            */
        }


        public void addTask(IMarketWorkUnit marketTask)
        {
            _marketTasks.Add(marketTask);
        }
        //for test purposes only, it creates few tasks 
        //for further processing over them
        public void initDefaultTasks()
        {

        }
        public void ProcessTasks()
        {
            foreach(var task in _marketTasks)
            {
                task.Process(_web);
                System.Threading.Thread.Sleep(2000);
            }
        }

        void IDisposable.Dispose()
        {
            _web.Dispose();
        }

      
    }
}
