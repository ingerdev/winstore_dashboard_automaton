﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winstore_dashboard_automaton.Helpers
{
    public static class WebDriverHelper
    {

        public static void WaitForElementLoad(this IWebDriver web, By by, int timeoutInSeconds=45)
        {
            if (timeoutInSeconds > 0)
            {
                WebDriverWait wait = new WebDriverWait(web, TimeSpan.FromSeconds(timeoutInSeconds));
                wait.Until(ExpectedConditions.ElementIsVisible(by));
            }
        }
        public static void FillFileDialog(this IWebDriver web,string filePath)
        {
            web.SwitchTo()
                    .ActiveElement()
                    .SendKeys(filePath);
            web.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(60));
        }

        public static void FillFileDialog2(this IWebDriver web, string filePath)
        {
            var alert = web.SwitchTo().Alert();

            // enter the filename
            alert.SendKeys(filePath);
        }
    }
}
