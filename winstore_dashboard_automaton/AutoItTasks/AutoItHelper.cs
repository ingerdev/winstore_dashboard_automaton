﻿using AutoIt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winstore_dashboard_automaton.AutoItTasks
{
    public static class AutoItHelper
    {
        public static void ChromeDriverSetFiledialogFilename(string fileName)
        {
            // var createdViaManifest = NRegFreeCom.ActivationContext.CreateInstanceWithManifest(new Guid("{1A671297-FA74-4422-80FA-6C5D8CE4DE04}"), "AutoItX3Dependency.manifest");
            // var autoItWithManifest = (AutoItX)createdViaManifest;
            // autoItWithManifest.Run("Notepad");
            AutoItX.WinWaitActive("Open");
            AutoItX.Send(fileName);
            AutoItX.Send("{ENTER}");
        }
    }
}
