# README #

Market store automatron. Your slave in painful world of microsoft store.

### What is this repository for? ###

* Quick summary  
Updating Windows market store app is a mess. Simply icon changing requires manual update for each language used.
App that inspired me create this automatron has 46 languages.

So, this app purpose is eliminate manual work.
How it works? It uses well-known Selenium and AutoIt.

Few page crawlers goes trough app names and submission directly to selected app's language list.
It iterates over app's languages block and execute custom actions on each language page.
Currently only one action is written - app icon changing.

New actions can be easily added as it require pretty small amount of work.

* Version
0.1